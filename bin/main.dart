import 'package:dslink/dslink.dart';
import 'package:dslink/nodes.dart';

import 'package:em_dslink_webscraper/src/profileActions.dart' as profile_action;
import 'package:em_dslink_webscraper/src/globals.dart' as globals;
import 'package:em_dslink_webscraper/src/rootNode.dart' as rootNode;


void main(List<String> args) {

  Map<String, dynamic> nodeProfiles = {
    ///////////////////////////
    // Remove is always first!
    ///////////////////////////

    'remove': (String path) =>
    new SimpleActionNode(path, (Map<String, dynamic> params) {
      Path parentPath = new Path(path).parent;

      Map<String, dynamic> removeResponse =
      profile_action.remove(parentPath.path);

      return removeResponse;
    }),

    ///////////////////////////
    // Root Profiles!
    ///////////////////////////

    'addClient' :(String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> addClientResponse =
      await profile_action.addClient(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return addClientResponse;

    }),

    'testHttp' : (String path) /////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> testHttpResponse =
      await profile_action.testHttp(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return testHttpResponse;

    }),

    'updatePostingRate' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic> changePostRateResponse =
      await profile_action.updatePostingRate(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return changePostRateResponse;

    }),

    'addPointReferences' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic>  addPointReferencesResponse =
      await profile_action.addPointReferences(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return addPointReferencesResponse;

    }),

    'cleanPoints' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic>  cleanPointsResponse =
      await profile_action.cleanPoints(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return cleanPointsResponse;

    }),

    'pointSelection' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic>  pointSelectionResponse =
      await profile_action.pointSelection(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return pointSelectionResponse;

    }),

    'publisherSettings' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic>  cleanPointsResponse =
      await profile_action.publisherSettings(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return cleanPointsResponse;

    }),

    'togglePosting' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic>  togglePostingResponse =
      await profile_action.togglePosting(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return togglePostingResponse;

    }),

    'pushData' : (String path) //////////////////////////////////////////
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;

      Map<String, dynamic>  response =
      await profile_action.pushData(parentPath.path, params)
          .catchError((error) {
        return {
          'succeeded' : false
        };
      });

      return response;

    }),

  };

  globals.link = new LinkProvider(args
      , "Modbus-"
      , isResponder: true
      , isRequester: true
      , encodePrettyJson: true
      , defaultNodes: rootNode.RootNode.definition
      , profiles: nodeProfiles
  );

  globals.provider = globals.link.provider;
  globals.requester = globals.link.requester;

  globals.link.init();

  globals.link.connect().whenComplete(() async {

  });


}
