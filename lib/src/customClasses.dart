import 'package:em_dslink_webscraper/src/globals.dart' as globals;
import 'package:em_dslink_webscraper/src/profileActionHelpers.dart' as helper;
import 'package:dslink/dslink.dart';

class TableValue {

  String Name;
  dynamic Value;
  String Unit;
  String TableName;

  TableValue(String tableName, String name, [dynamic value = null, String unit = null]) {

    TableName = tableName;
    Name = name;
    Value = value;
    if (unit != null) Unit = unit;

  }

}

class PathReference {

  String Original;
  String New;
  ReqSubscribeListener listener;

  PathReference(String originalPath, String newPath) {
    Original = originalPath;
    New = newPath;
  }

  SetListener(){
    LocalNode referenceNode = globals.provider.getNode(New);

    listener = globals.requester.subscribe(Original, (ValueUpdate update) {

      double numberValue = helper.doubleValueBasedOnTableValue(update.value);

      referenceNode.updateValue(numberValue);
    });
  }

  CancelListener() async {
    await listener.cancel();
  }
}