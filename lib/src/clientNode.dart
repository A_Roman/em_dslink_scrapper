import 'package:dslink/dslink.dart';
import 'package:em_dslink_webscraper/src/iotHubIntegration.dart' as iot;
import 'package:em_dslink_webscraper/src/customClasses.dart' as custom_classes;
import 'package:em_dslink_webscraper/src/profileActionHelpers.dart' as helper;
import 'package:em_dslink_webscraper/src/globals.dart' as globals;

import 'package:http/http.dart' as http;
import 'dart:async';

class ClientNode extends SimpleNode {


  Map<String, dynamic> _branchActions = {

    'addPoint': {
      r'$name': 'Add Point',
      r'$invokable': 'write',

      r'$params': [
        {'name': 'name', 'type': 'string'},
        {'name': 'initValue', 'type': 'number'},
      ],

      r'$results': 'values',
      r'$columns': [{'name': 'succeeded', 'type': 'bool'}],

      r'$is': "addPoint"
    },

    'togglePosting': {
      r'$name': 'Toggle Update',
      r'$invokable': 'write',

      r'$results': 'values',
      r'$columns': [{'name': 'succeeded', 'type': 'bool'}],

      r'$is': 'toggleUpdate'
    }
  };

  Map<String, dynamic> _baseTreeActionsMap = {
    "changeUpdateRate": {
      r'$name': 'Change Update Rate',
      r'$invokable': 'write',

      r'$params': [{'name': 'rateInMinutes', 'type': 'number'}],

      r'$result': 'values',
      r'$columns': [{'name': 'succeeded', 'type': 'bool'}],

      r'$is': 'changeUpdateRate'
    },

    "repopulate": {
      r'$name': 'Re Populate',
      r'$invokable': 'write',

      r'$result': 'values',
      r'$columns': [{'name': 'succeeded', 'type': 'bool'}],

      r'$is': 'repopulate'
    }
  };


  Map<String, dynamic> _removeAction = globals.removeAction;

  String nodeRootPath;

  //// SET ON INITIALIZATION
  String ipAddress;
  String name;

  //// UNIQUE INFORMATION
  bool subscription = true;
  bool posting = false;

  // stuff needed for data into EM.Core
  String userKey = 'userKey';
  String deviceKey = 'deviceKey';
  String iotDeviceId = 'iotdeviceKey';
  String iotHubUri = 'iotHubUri';
  String iotDeviceToken = 'iotDeviceToken';

  //// place holder to transfer data
  Map<String, dynamic> nodeMap;

  /// Constructor requires a string to build path for class, optional parameters are
  /// the device or the ipAddressAndPort separated by colon
  ClientNode(String ip, String clientName) : super('/$clientName') {

    nodeRootPath = '/$clientName';
    ipAddress = ip;
    name = clientName;

  }

  Map<String, dynamic> buildTree() {

    Map<String, dynamic> clientNodeMap = {
      r'$name': name,
      r'$is': 'deviceRoot'
    };


    Map<String, dynamic> clientAttributes = {
      '@ipAddress'        : ipAddress,

      '@initialConn'      : new DateTime.now().toUtc().toIso8601String(),
    };

    clientNodeMap.addAll(clientAttributes);

    clientNodeMap.addAll(_removeAction);

    nodeMap = clientNodeMap;

    globals.link.addNode("/$name", nodeMap);

    //////////////////////////////
    // now create flat list node
    //////////////////////////////
    Map<String,dynamic> flatListNodeMap = {
      r'$name' : 'Flat List',

      '@posting'          : false,

      '@userKey'          : userKey,
      '@deviceKey'        : deviceKey,
      '@iotHubUri'        : iotHubUri,
      '@iotHubDeviceId'   : iotDeviceId,
      '@iotHubDeviceToken': iotDeviceToken,

      r'$is' : 'deviceRoot'
    };

    // adding actions under flat list

    Map<String,dynamic> addPointReferences = { 'addPointReferences' : {
      r'$name' : 'Add Points References',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name' : 'referencePath',
          'type' : 'string'
        }
      ],

      r'$result' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'addPointReferences',
    }};

    flatListNodeMap.addAll(addPointReferences);

    Map<String,dynamic> pointSelection = { 'pointSelection' : {
      r'$name' : 'Point Selection',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name' : 'points',
          'type' : 'array'
        }
      ],

      r'$result' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'pointSelection'
    }};

    flatListNodeMap.addAll(pointSelection);

    Map<String, dynamic> publisherSettingsNode = { 'publisherSettings' : {
      r'$name' : 'Add Publisher Settings',
      r'$invokable' : 'write',

      r'$params' : [
        {
          'name' : 'userKey',
          'type' : 'string'
        },{
          'name' : 'deviceKey',
          'type' : 'string'
        },{
          'name' : 'iotHubUri',
          'type' : 'string'
        },{
          'name' : 'iotHubDeviceId',
          'type' : 'string'
        },{
          'name' : 'iotHubDeviceToken',
          'type' : 'string'
        }
      ],

      r'$result' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'publisherSettings',
    }};

    flatListNodeMap.addAll(publisherSettingsNode);

    Map<String,dynamic> togglePostingNode = { 'togglePosting' : {
      r'$name' : 'Toggle Posting',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'togglePosting'

    }};

    flatListNodeMap.addAll(togglePostingNode);

    Map<String,dynamic> cleanPointsNode = { 'cleanPoints' : {
      r'$name' : 'Clean Points',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'cleanPoints'

    }};

    flatListNodeMap.addAll(cleanPointsNode);

    Map<String,dynamic> pushDataNode = { 'pushData' : {
      r'$name' : 'Push Data',
      r'$invokable' : 'write',

      r'$results' : 'values',
      r'$columns' : [
        {
          'name' : 'succeeded',
          'type' : 'bool'
        }
      ],

      r'$is' : 'pushData'

    }};

    flatListNodeMap.addAll(pushDataNode);

    globals.link.addNode("/$name/${name}flat", flatListNodeMap);

    clientNodeMap.addAll(flatListNodeMap);
    nodeMap = clientNodeMap;

    return nodeMap;
  }

  Future<Map<String,dynamic>> addNodeBranches(Map<String,String> pathBranches) async {

    Map<String, dynamic> baseNodeMap = {
      r'$name': '${name} Root',
      r'$is'  : 'deviceRoot',

      '@updateRate' : 1
    };

    Map<String, dynamic> updateActionMap = _baseTreeActionsMap;

    baseNodeMap.addAll(updateActionMap);

    globals.link.addNode("/$name/${name}tree", baseNodeMap);
    globals.link.save();

    for(String pathName in pathBranches.keys) {

      http.Response httpResponse = await http.get("http://$ipAddress/${pathBranches[pathName]}");

      if (httpResponse.statusCode != 200) throw new Exception("Failed to communicate with client");

      List<custom_classes.TableValue> rawValues;

      try {
        rawValues = helper.getRawTableValues(httpResponse.body);
      } catch (exception) {
        rawValues = [];
//        throw new Exception("Couldnt parse address :: http://$ipAddress/${pathBranches[pathName]}");
      }

      // create branch
      Map<String, dynamic> branchNodeMap = {
        r'$name': pathName,
        r'$is': 'deviceRoot',

        '@webPath' : 'http://$ipAddress/${pathBranches[pathName]}',
        '@autoUpdate' : true

      };

      // add action and whatnot
      branchNodeMap.addAll(_branchActions);

      globals.link.addNode("/$name/${name}tree/$pathName", branchNodeMap);
      globals.link.save();

      for (custom_classes.TableValue rawValue in rawValues){

        Map<String,dynamic> pointNodeMap = {
          r'$name' : rawValue.Name,
          r'$is'   : rawValue.Name.replaceAll(" ",''),
          '@reporting' : true
        };

        try {
          double numberValue = double.parse(rawValue.Value.toString());

          pointNodeMap.addAll({
            r'$type' : 'number',
            '?value' : numberValue,
          });
        } catch (exception) {
          pointNodeMap.addAll({
            r'$type' : 'string',
            '?value' : rawValue.Value,
          });
        }

        pointNodeMap.addAll(_removeAction);

        globals.link.addNode("/$name/${name}tree/$pathName/${rawValue.Name.replaceAll(" ",'')}", pointNodeMap);
        globals.link.save();

        branchNodeMap.addAll({
          rawValue.Name.replaceAll(" ",'') : pointNodeMap
        });

      }

      baseNodeMap.addAll({
        pathName : branchNodeMap
      });

    }

    nodeMap.addAll({
      "treeRoot" : baseNodeMap
    });

    return nodeMap;

  }

  Future<bool> updatePointValues(LinkProvider link) async {

    bool success = await updateNodePointValues(link, name);

    return success;

  }

}


Future<bool> updateNodePointValues(LinkProvider link, String uuid) async {

  int deviceConnectionStatus = 1;
  bool response = true;

  LocalNode lNode = link.getNode('/$uuid');
  LocalNode lRootNode = link.getNode('/');

//  // get state val
//  wemo_calls.GetStateCall getStateCall =
//  new wemo_calls.GetStateCall(ipAddress, port, defaultPorts);
//
//  await getStateCall.callWemo();
//
//  if (getStateCall.succeeded) {
//    link.val('/$uuid/stateValue', getStateCall.binaryState);
//  }
//
//  // get signal strength
//  wemo_calls.GetSignalStrengthCall getSignalStrengthCall =
//  new wemo_calls.GetSignalStrengthCall(ipAddress, port, defaultPorts);
//
//  await getSignalStrengthCall.callWemo();
//
//  if (getSignalStrengthCall.succeeded) {
//    link.val('/$uuid/signalStrength', getSignalStrengthCall.signalStrength);
//  }
//
//  // get firmware version
//  wemo_calls.GetFirmwareVersionCall getFirmwareVersionCall =
//  new wemo_calls.GetFirmwareVersionCall(ipAddress, port, defaultPorts);
//
//  await getFirmwareVersionCall.callWemo();
//  getFirmwareVersionCall.getFirmwareDouble();
//
//  if (getFirmwareVersionCall.succeeded) {
//    link.val('/$uuid/lightswitchFirmwareVersion', getFirmwareVersionCall.firmwareVersionDouble);
//  }
//
//  // if both calls failed then device connection status is 0 and we were unable to connec to device
//  if (!getStateCall.succeeded && !getSignalStrengthCall.succeeded && !getFirmwareVersionCall.succeeded){
//    deviceConnectionStatus = 0;
//    response = false;
//  }
//
//  link.val('/$uuid/deviceConnectionStatus', deviceConnectionStatus);

  return response;

}

Future<bool> httpUpdate(LinkProvider link, String uuid) async {

  LocalNode lNode = link.getNode('/$uuid');
  LocalNode rootNode = link.getNode('/');

  // needed stuff for interaction to IoTHub
  String iotHubUri = lNode.getAttribute('@iotHubUri');
  String iotDeviceId = lNode.getAttribute('@iotHubDeviceId');
  String iotDeviceToken = lNode.getAttribute('@iotHubDeviceToken');
  int minsToExpiration = int.parse(rootNode.get('@sasTokenExpirationInMin').toString());

  // needed for data to make it to the appropiate asset
  String userKey = lNode.getAttribute('@userKey');
  String deviceKey = lNode.getAttribute('@deviceKey');

  String connectorName = "DSA-WeMo";

  var stateValue = link.val('/$uuid/stateValue');
  var signalStrength = link.val('/$uuid/signalStrength');
  var deviceConnectionStatus = link.val('/$uuid/deviceConnectionStatus');
  var lightswitchFirmwareVersion = link.val('/$uuid/lightswitchFirmwareVersion');

  Map<String,dynamic> points = {
    'stateValue': stateValue,
    'signalStrength': signalStrength,
    'deviceConnectionStatus': deviceConnectionStatus,
    'lightswitchFirmwareVersion': lightswitchFirmwareVersion
  };

  String sasToken = iot.generateSaSToken( iotHubUri, iotDeviceToken, iotDeviceId, minsToExpiration);
  Map<String, dynamic> payload = iot.buildPayload(iotDeviceId, userKey, deviceKey, connectorName, points);
  int statusCode = await iot.httpPost(sasToken, iotHubUri, payload);

  bool response = (statusCode == 204) ? true : false;

  return response;

}