import 'dart:async';

import 'package:dslink/dslink.dart';

import 'package:http/http.dart' as http;

import 'package:em_dslink_webscraper/src/globals.dart' as globals;
import 'package:em_dslink_webscraper/src/profileActionHelpers.dart' as helper;
import 'package:em_dslink_webscraper/src/verifyInputs.dart' as verify;
import 'package:em_dslink_webscraper/src/clientNode.dart' as clientNode;
import 'package:em_dslink_webscraper/src/customClasses.dart' as custom_classes;

/// [###] Removed existing node from structure
Map<String,dynamic> remove(String pathOfParent) {

  bool response = false;

  LocalNode lNode = globals.link.getNode(pathOfParent);

  if (lNode.get(r'$is') == 'referenceValue') {

    String path = lNode.get('@originalPath');

    // stop subscribers
    if (globals.allReferences[path] != null) {
      globals.allReferences[path].CancelListener();
      globals.allReferences.remove(path);
    }
  }

  try {

    globals.link.removeNode(lNode.path);
    globals.link.save();

    response = true;

  } catch (ex) {

    return {
      'succeeded' : response,
    };

  }

  return {
    'succeeded' : response,
  };
}

///////////////////////////
// Root Profiles!
///////////////////////////

/// [DONE] add device from specific ip (will use specified ports of interest to connect)
Future<Map<String,dynamic>> addClient(String pathOfParent, Map<String, dynamic> params) async {

  bool succeeded = false;

  String ip = params['ip'];
  String name = params['name'];

  List<String> existingClients = [];

  globals.link.getNode(pathOfParent).children.forEach( (String nodeName, Node node) {
    if(node.get(r'$is') == 'deviceRoot') {
      existingClients.add(nodeName);
    }
  });

  if(!verify.validIp(ip)){
    return {
      'succeeded' : succeeded,
    };

  };

  bool failedResponse = false;
  http.Response response;
  await http.get("http://$ip").timeout( new Duration( seconds: 30), onTimeout: () {
    failedResponse = true;
  }).then( (http.Response responseInBody) {
    response = responseInBody;
  });

  if (failedResponse)
  {
    return {
      'succeeded' : succeeded,
    };
  }

  var mainBody = response.body;

  if (mainBody.contains("Emerson")) {

    clientNode.ClientNode client = new clientNode.ClientNode(ip,name);
    client.buildTree();
    await client.addNodeBranches(globals.nameToPathMap);

    succeeded = true;
  } else {
    succeeded = false;
  }

  return {
    'succeeded' : succeeded,
  };

}

Future<Map<String, dynamic>> updateSasTokenExpiration(String pathOfParent, Map<String, dynamic> params) async {

  bool succeeded = false;

  LocalNode lNode = globals.link.getNode(pathOfParent);

  int postingRate = int.parse(params['sasTokenExpirationInMin'].toString());

  lNode.attributes['@sasTokenExpirationInMin'] = postingRate;
  lNode.listChangeController.add('@sasTokenExpirationInMin');

  globals.link.save();
  succeeded = true;

  return {
    'succeeded' : succeeded
  };


}

Future<Map<String, dynamic>> testHttp(String pathOfParent, Map<String, dynamic> params) async {

  bool succeeded = false;

  String urlEndpoint = params['targetUri'];
  var headerIn = params['headers'];
  String body = params['body'];

  Uri postUrl = Uri.parse(urlEndpoint);
  Map<String, dynamic> callHeaders = helper.mapParser(headerIn);

  http.Response response = await http.post(postUrl, headers: callHeaders, body: body);

  succeeded = true;

  return {
    'succeeded' : succeeded,
    'statusCode' : response.statusCode
  };

}

Future<Map<String, dynamic>> updatePostingRate(String pathOfParent, Map<String, dynamic> params) async {

  bool succeeded = false;

  LocalNode lNode = globals.link.getNode(pathOfParent);

  double postingRate = double.parse(params['postRateInMin']);

  lNode.attributes['@postRateInMin'] = postingRate;
  lNode.listChangeController.add('@postRateInMin');

  try {

    globals.timer.cancel();

    var newTimerDuration = new Duration(seconds: (postingRate*60).floor());
    globals.timer = new Timer.periodic(newTimerDuration, (_) async {
      await globals.updateFunction(globals.link);
    });

    globals.link.save();
    succeeded = true;

  } catch (e) {

    return {
      'succeeded': succeeded
    };

  }

  return {
    'succeeded' : succeeded
  };


}

Future<Map<String, dynamic>> addPointReferences(String pathOfParent, Map<String, dynamic> params) async {

  bool succeeded = false;

  String referencePath = params['referencePath'];

  LocalNode lNode = globals.link.getNode(pathOfParent);
  Map<String,SimpleNode> currentChildren = lNode.children;
  List<String> existingReferences = [];

  for (SimpleNode sNode in currentChildren.values){
    if (sNode.get(r'$is') != null) {
      String originalPath = sNode.get('@originalPath');
      existingReferences.add(originalPath);
    }
  }

  SimpleNode lNodeOfReferenceParent = globals.link.getNode(referencePath);

  Map<String,SimpleNode> childNodes = lNodeOfReferenceParent.children;

  for (SimpleNode childNode in childNodes.values){

    dynamic nodeValue = childNode.value;

    if (nodeValue != null && !existingReferences.contains(childNode.path)) {
      double doubleValue = helper.doubleValueBasedOnTableValue(childNode.value);
      String name = childNode.name;

      Map<String, dynamic> pointNodeMap = {
        r'$name' : name,
        r'$type' : "number",

        '@originalPath' : childNode.path,
        '@reporting' : true,

        '?value' : doubleValue,

        r'$is' : 'referenceValue'
      };

      custom_classes.PathReference reference = new custom_classes.PathReference(childNode.path, '${pathOfParent}/${name}');
      reference.SetListener();

      globals.allReferences.addAll({
        childNode.path : reference
      });

      pointNodeMap.addAll(globals.removeAction);

      globals.link.addNode(reference.New, pointNodeMap);
    }
  }

  succeeded = true;

  return {
    'succeeded' : succeeded
  };
}

Future<Map<String, dynamic>> cleanPoints(String pathOfParent, Map<String, dynamic> params) async {

  bool succeeded = false;

  SimpleNode lNode = globals.link.getNode(pathOfParent);

  Map<String,SimpleNode> childNodes = lNode.children;

  List<String> childPaths = [];
  childNodes.forEach((name, sNode) => childPaths.add(sNode.path));

  for (String childPath in childPaths){

    SimpleNode sNode = globals.link.getNode(childPath);

    if (sNode.get(r'$invokable') == null) {

      Map<String, dynamic> response = remove(childPath);

      if (!response.values.first) print('failed to delete : ${childPath}');

    }

  }

  succeeded = true;

  return {
    'succeeded' : succeeded
  };
}

/// [###] add publisher settings where date is being pushed
Future<Map<String,dynamic>> publisherSettings(String pathOfParent, Map<String, dynamic> params) async {

  LocalNode lNode = globals.link.getNode(pathOfParent);

  params.forEach((attributeName,newStringValue) {

    params[attributeName].toString().toLowerCase();
    lNode.attributes['@$attributeName'] = newStringValue;
    lNode.listChangeController.add('@$attributeName');

  });

  lNode.attributes['@posting'] = true;
  lNode.listChangeController.add('@posting');

  bool updateState = true;

  bool postState = false;

  try{
    postState = await helper.postToIotHub(lNode);
  } catch (exception, stacktrace) {
    throw new Exception("Error pushing data to IoT Hub: ${exception} \n $stacktrace");
  }

  bool response = (updateState || postState) ? true : false;

  globals.link.save();

  return{
    'succeeded' : response
  };

}

Future<Map<String,dynamic>> pointSelection(String pathOfParent, Map<String, dynamic> params) async {

  bool response = false;

  List<String> selectedPoints = helper.arrayParser(params["points"]);

  LocalNode lNode = globals.link.getNode(pathOfParent);

  for(SimpleNode child in lNode.children.values){

    // child with values
    if(child.value != null) {

      if (selectedPoints.contains(child.name))
      {
        child.attributes['@reporting'] = true;
      } else {
        child.attributes['@reporting'] = false;
      }
      child.listChangeController.add('@reporting');

    }
  }

  response = true;

  return{
    'succeeded' : response
  };

}

Future<Map<String,dynamic>> togglePosting(String pathOfParent, Map<String, dynamic> params) async {

  bool response = false;

  LocalNode lNode = globals.link.getNode(pathOfParent);
  bool currentStateSubscription = (lNode.get('@posting').toString().toLowerCase() == 'true');

  lNode.attributes['@posting'] = !currentStateSubscription;
  lNode.listChangeController.add('@posting');

  try {

    globals.link.save();
    response = true;

  } catch (e) {

    return {
      'succeeded': response
    };

  }

  return {
    'succeeded' : response
  };

}

Future<Map<String,dynamic>> pushData(String pathOfParent, [Map<String, dynamic> params]) async {

  LocalNode lNode = globals.link.getNode(pathOfParent);
  bool response = await helper.postToIotHub(lNode);

  return {
    'succeeded' : response
  };

}