import 'dart:async';
import 'package:dslink/dslink.dart';
import 'package:em_dslink_webscraper/src/customClasses.dart' as custom_classes;


bool debuggingLog = false;
Timer timer = new Timer.periodic(null, null);

Map<String, custom_classes.PathReference> allReferences = {};
Map<String, Timer> websiteRefreshers = {};

SimpleNodeProvider provider;
Requester requester;
LinkProvider link;

Map<String, dynamic> removeAction = {
  'remove': {
    r'$name': 'Remove',
    r'$invokable': 'write',

    r'$result': 'values',
    r'$columns': [{'name': 'succeeded', 'type': 'bool'}],

    r'$is': 'remove'
  }
};

Map<String,String> nameToPathMap = {
  "battery" : "monitor/upsBattery.htm",
  "alarms" : "monitor/deviceActiveAlarms.htm",
  "bypass":"monitor/upsBypass.htm",
  "input" : "monitor/upsInput.htm",
  "rectifier" : "monitor/upsRectifier.htm",
  "inverter" : "monitor/upsInverter.htm",
  "output": "monitor/upsOutput.htm",
  "system" : "monitor/upsSystem.htm"
};

class EmersonUps {

  String IpAddress;

  EmersonUps(String clientIp){
    IpAddress = clientIp;
  }

  String Battery() {return nameToPathMap["battery"];}
  String Alarms() {return nameToPathMap["alarms"];}
  String Bypass() {return nameToPathMap["bypass"];}
  String Input() {return nameToPathMap["input"];}
  String Rectifier() {return nameToPathMap["rectifier"];}
  String Output() {return nameToPathMap["output"];}
  String System() {return nameToPathMap["system"];}


}


updateFunction(LinkProvider link) async {
  debuggingTool('webjob started at ${new DateTime.now()}');

  Map<String, dynamic> children = link.getNode('/').children;

  Map<String, SimpleNode> listOfDevice = {};

  children.forEach((String key, var node) {
    String nodeIs = node.get(r'$is');

    if (nodeIs == 'deviceRoot') {
      listOfDevice.addAll({key: node});
    }
  });

  // TODO, fix when i have data parsing doen
//  for (String key in listOfDevice.keys) {
//    if (children[key].getAttribute('@subscription').toString() == 'true') {
//      globals.debuggingTool("subscription for $key started");
//
//      await wemo_node.updateNodePointValues(link, key).then((success) {
//        globals.debuggingTool(
//            "subscription for ${key} ${(success) ? "successfull" : "failed"}");
//      }).catchError((ex) {
//        globals.debuggingTool("$ex");
//      });
//    }
//
//    if (children[key].getAttribute('@posting').toString() == 'true') {
//      globals.debuggingTool("posting for $key started");
//
//      await wemo_node.httpUpdate(link, key).then((success) {
//        globals.debuggingTool(
//            "posting for ${key} ${(success) ? "successfull" : "failed"}");
//      }).catchError((ex) {
//        globals.debuggingTool("${ex}");
//      });
//    }
//  }
}

debuggingTool(var message) {
  if (debuggingLog) {
    print("DEBUG (${new DateTime.now()}) :: ${message}");
  }
}