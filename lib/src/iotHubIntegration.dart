import 'package:em_dslink_webscraper/src/globals.dart' as globals;

import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';
import 'dart:async';
import 'package:crypto/crypto.dart';
import 'dart:convert';

Future<int> httpPost(String sas, String endPoint, Map<String,dynamic> payload) async {

  String deviceId = payload["DeviceId"];

  Uri postUrl = Uri.parse('https://$endPoint/devices/$deviceId/messages/events?api-version=2016-11-14');

  String guid = new Uuid().v4();

  Map<String,String> callHeaders = {
    'Authorization'     : sas,
    'IoTHub-MessageId'  : guid
  };

  payload.addAll( {
    "SizeInBytes": UTF8.encode(JSON.encode(payload)).length
  });

  http.Response response = await http.post(postUrl, headers: callHeaders, body: JSON.encode(payload));

  globals.debuggingTool("Status Code :: ${response.statusCode} for UUID : ${payload['AssetKey']}");
  globals.debuggingTool(callHeaders);
  globals.debuggingTool(payload);

  return response.statusCode;

}

Map<String,dynamic> buildPayload(String deviceId, String userKey, String assetKey, String connectorName , Map<String,dynamic> points) {

  String utcTimeNow = new DateTime.now().toUtc().toIso8601String();

  List<Map<String, dynamic>> formattedPoints = [];


  for (String valueName in points.keys) {
    Map<String, dynamic> formattedPoint = {
      'Name'  : valueName,
      'Value' : points[valueName]
    };
    formattedPoints.add(formattedPoint);
  }

  Map<String, dynamic> payload = {
    "DeviceId": deviceId,
    "UserKey": userKey,
    "AssetKey": assetKey,
    "ConnectorName": connectorName,
    "Points": formattedPoints,
    "TimeStamp": utcTimeNow
  };

  return payload;
}

String generateSaSToken(String uri, String iotHubDeviceKey, String iotHubDeviceId, int minsToExpiration ,{String policyName = null} ) {

  // full Uri
  String encodedUri = Uri.encodeComponent('$uri/devices/$iotHubDeviceId').toLowerCase();

  // set expiration in seconds
  DateTime timeOfExpiration = new DateTime.now().add( new Duration( minutes: minsToExpiration));
  int timeOfExpiryInSeconds = (timeOfExpiration.millisecondsSinceEpoch / 1000).ceil();

  // create signing string
  String stringToSign = '${encodedUri}\n${timeOfExpiryInSeconds}';

  String signature;

  // use crypto
  try {
    Hmac hmac = new Hmac(sha256, BASE64.decode(iotHubDeviceKey));
    Digest digest = hmac.convert(UTF8.encode(stringToSign));
    signature = BASE64.encode(digest.bytes);
  } catch (exception, stacktrace) {
    throw new Exception("Error");
  }

  // construct authorization string
  String token = 'SharedAccessSignature sr=${encodedUri}&sig=${Uri.encodeComponent(signature)}&se=${timeOfExpiryInSeconds}';
  if (policyName != null) {
    token = '${token}&skn=${policyName}';
  }

  return token;

}