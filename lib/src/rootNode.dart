

class RootNode {

  static Map<String, dynamic> definition = {
    r'$name': 'WeMo DSLink Root', r'$is': 'DSLink',

    '@defaultDeviceManager':
        'https://EMDev.azure-devices.net/devices/|DeviceID|/messages/events?api-version=2016-11-14',

    '@postRateInMin': 10, '@sasTokenExpirationInMin': 5,

    // Add client
    'addClient': {
      r'$name': 'Add Client',
      r'$invokable': 'config',
      r'$params': [
        {'name': 'ip', 'type': 'string', 'default' : "10.9.1.192"},
        {'name': 'name', 'type': 'string', 'default' : 'test'}
      ],
      r'$result': 'values',
      r'$columns': [
        {'name': 'succeeded', 'type': 'bool'}
      ],
      r'$is': 'addClient',
    },

    // (change attributes) update postRate
    'updatePostingRate': {
      r'$name': 'Change Post Rate',
      r'$invokable': 'config',
      r'$params': [
        {'name': 'postRateInMin', 'type': 'number'}
      ],
      r'$result': 'values',
      r'$columns': [
        {'name': 'succeeded', 'type': 'bool'}
      ],
      r'$is': 'updatePostingRate'
    },

    // (change attributes) update length of SaS token expiration
    'updateSasTokenExpiration': {
      r'$name': 'Change SaS Token Expiration',
      r'$invokable': 'config',
      r'$params': [
        {'name': 'sasTokenExpirationInMin', 'type': 'number'}
      ],
      r'$result': 'values',
      r'$columns': [
        {'name': 'succeeded', 'type': 'bool'}
      ],
      r'$is': 'updateSasTokenExpiration'
    },

    // test http connection to anything
    'testHttp': {
      r'$name': 'Test Http Connection',
      r'$invokable': 'config',
      r'$params': [
        {
          'name': 'targetUri',
          'type': 'string',
        },
        {
          'name': 'headers',
          'type': 'map',
        },
        {
          'name': 'body',
          'type': 'string',
        }
      ],
      r'$result': 'values',
      r'$columns': [
        {'name': 'succeeded', 'type': 'bool'},
        {'name': 'statusCode', 'type': 'number'}
      ],
      r'$is': 'testHttp'
    },
  };

}
