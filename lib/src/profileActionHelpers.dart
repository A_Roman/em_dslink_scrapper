import 'dart:convert';
import 'package:petitparser/json.dart' as petit_parser;
import 'package:dslink/dslink.dart';
import 'package:em_dslink_webscraper/src/customClasses.dart' as custom_classes;
import 'package:em_dslink_webscraper/src/globals.dart' as globals;
import 'package:http/http.dart' as http;
import 'package:html/parser.dart' as html_parser;
import 'package:html/dom.dart' as html_dom;
import 'package:em_dslink_webscraper/src/iotHubIntegration.dart' as iot;
import 'dart:async';


Map<String, dynamic> mapParser(var variableMap) {
  Map<String, dynamic> mapReturn = {};

  petit_parser.JsonParser jsonPetitParser = new petit_parser.JsonParser();

  if (variableMap is String) {
    var jsonPetitParserResponse = jsonPetitParser.parse(variableMap);

    if (jsonPetitParserResponse.isFailure) {
      mapReturn = JSON.decode(variableMap);
    } else {
      mapReturn = jsonPetitParserResponse.value;
    }
  } else if (variableMap is Map) {
    mapReturn = variableMap;
  }

  return mapReturn;
}

List<dynamic> arrayParser(var variableArray) {
  List<dynamic> arrayReturn = [];

  petit_parser.JsonParser jsonPetitParser = new petit_parser.JsonParser();

  if (variableArray is String) {
    var jsonPetitParserResponse = jsonPetitParser.parse(variableArray);

    if (jsonPetitParserResponse.isFailure) {
      arrayReturn = JSON.decode(variableArray);
    } else {
      arrayReturn = jsonPetitParserResponse.value;
    }
  } else if (variableArray is List<dynamic>) {
    arrayReturn = variableArray;
  }

  return arrayReturn;
}

List<String> getListOfIps(
    String startIp, String endIp, List<dynamic> listOfSubnets) {
  List<String> tempListOfIps = [];
  List<String> listOfFinalIps = [];

  int ipStart = int.parse(startIp.split('.').last);
  int ipEnd = int.parse(endIp.split('.').last);

  String tempSubnet = startIp.split('.').sublist(0, 3).join('.');

  while (ipStart <= ipEnd) {
    tempListOfIps.add([tempSubnet, ipStart.toString()].join('.'));

    ipStart++;
  }

  // if wildcard is present.... we will replace the x with subnets values...
  if (startIp.toLowerCase().contains('x') &&
      endIp.toLowerCase().contains('x')) {
    for (dynamic subnet in listOfSubnets) {
      tempListOfIps.forEach((String ipWithWildcard) {
        listOfFinalIps.add(
            ipWithWildcard.toLowerCase().replaceAll('x', subnet.toString()));
      });
    }

    // else just pass the existing list....
  } else {
    listOfFinalIps = tempListOfIps;
  }

  return listOfFinalIps;
}

List<String> getListOfExistingUuid(LinkProvider link) {
  LocalNode lNode = link.getNode('/');

  List<String> existingUuidList = [];

  lNode.children.values.forEach((Node childNode) {
    if (childNode.getConfig(r'$is') == 'deviceRoot') {
      existingUuidList.add(childNode.getConfig(r'$name'));
    }
  });

  return existingUuidList;
}

List<SimpleNode> getValuedChildNodes (SimpleNode rootNode, [bool getReportingOnly = false]) {
  List<SimpleNode> childNodes = rootNode.children.values;
  List<SimpleNode> valuedNodes = [];

  for (SimpleNode child in childNodes) {
    if (child.value != null) {
      bool include = true;

      if (getReportingOnly) {
        include = child.getAttribute('@reporting');
      }

      if (include) {
        valuedNodes.add(child);
      }
    }
  }
}

Future<bool> updateNodeFromTableSource(SimpleNode branchNode, [bool createNewPoints = false]) async {

  List<SimpleNode> valuedNodes = getValuedChildNodes(branchNode);

  String webPath = branchNode.getAttribute("@webPath");

  http.Response httpResponse = await http.get(webPath);

  if (httpResponse.statusCode != 200) throw new Exception("Failed to communicate with client at $webPath");

  List<custom_classes.TableValue> tableValues = [];

  try {
    tableValues = getRawTableValues(httpResponse.body);
  } catch (exception) {
    throw new Exception("Couldnt parse tables at address :: $webPath");
  }

  for (custom_classes.TableValue tableValue in tableValues) {

    String expectedPointName = tableValue.Name.replaceAll(" ",'');
    String expectedPointPath = '${branchNode.path}/${expectedPointName}';

    // value is either number or string

    String numberValueType;

    try{
      tableValue.Value = int.parse(tableValue.Value);
      numberValueType = "number";
    } catch (expected) {
      tableValue.Value = tableValue.Value;
      numberValueType = "string";
    }

    double numberValue = double.parse(tableValue.Value.toString());

    if (createNewPoints) {
      Map<String, dynamic> pointNodeMap = {
        r'$name': tableValue.Name,
        r'$is': expectedPointName,
        '@reporting': true
      };

      // depending on value type
      if (numberValueType == "number"){
        pointNodeMap.addAll({
          r'$type': 'number',
          '?value': numberValue,
        });
      }
      else if (numberValueType == "string")
      {
        pointNodeMap.addAll({
          r'$type': 'string',
          '?value': tableValue.Value,
        });
      }

      globals.link.addNode(expectedPointPath, pointNodeMap);
      globals.link.save();
    }
    else{
      globals.link.updateValue(expectedPointName, numberValue);
    }

    globals.link.save();
  }
}

List<custom_classes.TableValue> getRawTableValues(String htmlString) {
  // parse document
  html_dom.Document doc = html_parser.parse(htmlString);

  //get details panel
  html_dom.Element detailPanelElement = doc.getElementById('DetailPanelArea');

  //get the table elements
  List<html_dom.Element> tableElements = detailPanelElement
      .getElementsByTagName("table");

  // return
  List<custom_classes.TableValue> pointRawValues = [];

  for (html_dom.Element tableElement in tableElements) {
    int rowElementIdx = 0;
    // gets rows

    bool hasUnits = false;
    bool hasValue = false;
    String tableName = "";

    List<html_dom.Element> rowElements = tableElement.getElementsByTagName('tr');

    for (html_dom.Element rowElement in rowElements) {
      // get row elements
      List<html_dom.Element> htmlRawElements = rowElement.children;

      // if header column or value
      if (rowElementIdx == 0) {
        tableName = htmlRawElements[1].text;

        if (htmlRawElements.length > 2) {
          hasValue = true;

          if (htmlRawElements.length > 3) {
            hasUnits = true;
          }
        }
      } else {
        custom_classes.TableValue classedValue;

        classedValue =
        new custom_classes.TableValue(tableName, htmlRawElements[1].text);

        if (hasValue) {
          classedValue.Value = htmlRawElements[2].text;
          if (hasUnits) {
            classedValue.Unit = htmlRawElements[3].text;
          }
        }
        pointRawValues.add(classedValue);
      }
      rowElementIdx++;
    };
  }

  return pointRawValues;
}

double doubleValueBasedOnTableValue(dynamic value) {

  double numberValue = -1.0;

  // could be string or boolean
  if(value.runtimeType == String) {

    String stringValue = value.toString().toLowerCase();

    if (stringValue == "false" || stringValue == 'normal' || stringValue == 'closed') {
      numberValue = 0.0;
    } else {
      numberValue = 1.0;
    }

  } else {
    numberValue = double.parse(value.toString());
  }

  return numberValue;
}

Future<bool> postToIotHub(SimpleNode node) async {

  // needed stuff for interaction to IoTHub
  String iotHubUri = node.getAttribute('@iotHubUri');
  String iotDeviceId = node.getAttribute('@iotHubDeviceId');
  String iotDeviceToken = node.getAttribute('@iotHubDeviceToken');
  int minsToExpiration = 5;

  // needed for data to make it to the appropiate asset
  String userKey = node.getAttribute('@userKey');
  String deviceKey = node.getAttribute('@deviceKey');

  String connectorName = "DSA-SNMP";

  Map<String,SimpleNode> children = node.children;

  Map<String,double> pointsMap = {};

  for(SimpleNode child in children.values) {

    if(child.value != null) {
      pointsMap.addAll(
        {child.name: child.value}
      );
    }
  }

  String sasToken;

  try{
    sasToken = iot.generateSaSToken( iotHubUri, iotDeviceToken, iotDeviceId, minsToExpiration);
  } catch(exception,stacktrace) {
    throw new Exception("Error creating sas token : $exception \n $stacktrace");
  }

  Map<String, dynamic> payload = iot.buildPayload(iotDeviceId, userKey, deviceKey, connectorName, pointsMap);
  int statusCode = await iot.httpPost(sasToken, iotHubUri, payload);

  bool response = (statusCode == 204) ? true : false;

  return response;

}